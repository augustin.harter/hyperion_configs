#!/usr/bin/env bash

######## TODO
#source ./paths.sh
export prefix="/vol/tiago/melodic-robocup/"
export PATH="${prefix}/bin:$PATH"

#ROS source alias
export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/tiago_clf_nav/data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"
##########

#Robot Setup
export basepc=$(hostname -s)
export laptop=${basepc}
export robot=${basepc}
export ROS_MASTER_URI=http://${basepc}:11311
export ROBOT_VERSION="steel"
export SIMMODE="true"

#Simulation map/world
export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_map.yaml"
export SIMULATION_WORLD="clf"

#Pocketsphinx_grammars TODO Exercise 5
export VDEMO_PSA_CONFIG="/home/aharter/robocup/ex5/robocup-speechrec/psConfig/exercise/aharter.conf"

#Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

# Default KBASE locations
export PATH_TO_MONGOD_CONFIG="$prefix/share/robocup_data/mongod/mongod.conf"
export PATH_TO_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/use_challenge_WS1920_db.yaml"
export PATH_TO_EDIT_KBASE_CONFIG="$prefix/share/robocup_data/knowledgeBase/configs/edit_challenge_WS1920_db.yaml"

#object
export PATH_TO_CLASSIFIERS="${prefix}/share/storing_groceries_node/object"
export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/detection/frozen_inference_graph.pb"
export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/detection/label_map.pbtxt"
export OBJECT_REC_PATH="${HOME}/robocup/robocup_challenge/robocup_data/tensorflow/output"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"

# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
